package com.example.carter.notificationdemo.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * 測試用空白頁
 */
class BlankActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent != null) {
            val x  = intent.getStringExtra("test")
            if (x == null) {
                finish()
                return
            }
        }
    }
}