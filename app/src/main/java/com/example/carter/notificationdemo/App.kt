package com.example.carter.notificationdemo

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initFresco()
        initNotificationChannel()
    }

    private fun initFresco() {
        val builder = ImagePipelineConfig.newBuilder(this)
            .setDownsampleEnabled(true)

        Fresco.initialize(this, builder.build())
    }

    private fun initNotificationChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }
        val id = Constants.NOTIFICATION_CHANNEL_ID
        val name = Constants.NOTIFICATION_CHANNEL_NAME
        val importance = NotificationManager.IMPORTANCE_DEFAULT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(id, name, importance)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}