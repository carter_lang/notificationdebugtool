package com.example.carter.notificationdemo.ui

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import com.example.carter.notificationdemo.Constants
import com.example.carter.notificationdemo.R

/**
 * 開啟此頁面直接發送一個圖片推播
 */
class PictureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        val icon = BitmapFactory.decodeResource(
            resources,
            R.drawable.pic_long
        )

        val builder = NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_ID)

        builder.setContentTitle("title")
            .setContentText("content")
            .setDefaults(Notification.DEFAULT_SOUND)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setChannelId(Constants.NOTIFICATION_CHANNEL_ID)
            .setAutoCancel(true)
            .setLargeIcon(icon)

        val jumpIntent = Intent(applicationContext, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext,0,jumpIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification = builder.build()
        notificationManager.notify(0, notification)
    }
}