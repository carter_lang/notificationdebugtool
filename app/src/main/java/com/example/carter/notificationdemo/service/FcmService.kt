package com.example.carter.notificationdemo.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.example.carter.notificationdemo.Constants
import com.example.carter.notificationdemo.R
import com.facebook.common.executors.UiThreadImmediateExecutorService
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequest
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FcmService : FirebaseMessagingService() {

    private val TAG = FcmService::class.java.simpleName

    /**
     * 資料範例：
     * {
     *  "to" : "esLUylLHTGM:APA91bHqpd9URbC8a7kTXIyT-osRuT-p3NHAoDBDCHIf9Rm0XUUpn3qVhcPn89npri5pY0nxpzC2cewWBn_PYVnCW-qCAArA8A-aqAv9cBHGbHfNasAYwBj6h2TsxrDWI0dAuvjEQ4Aw",
     *  "data" : {
     *      "body" : "Body of Your Notification in Data",
     *      "title": "Title of Your Notification in Title",
     *      "key1" : "Value for key_1",
     *      "key_2" : "Value for key_2",
     *      "pic" : "https://image.shutterstock.com/image-photo/fashion-photo-beautiful-young-woman-260nw-500356678.jpg"
     *  }
     * }
     */

    override fun onMessageReceived(msg: RemoteMessage) {

        val data = msg.data
        val notification = msg.notification
        val title = notification?.title
        val msg = notification?.body

        val url = data["pic"]
        if (url != null) {
            Log.e(TAG, "pic=$url")
            val intent = Intent().apply {
                putExtra("url", url)
            }
            FetchPictureService.enqueueWork(this, intent)
        }
    }

    override fun onNewToken(token: String) {
        Log.e(TAG, "token=$token")
    }
}