package com.example.carter.notificationdemo.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v4.app.JobIntentService
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.example.carter.notificationdemo.Constants
import com.example.carter.notificationdemo.R
import com.example.carter.notificationdemo.ui.MainActivity
import com.facebook.common.executors.UiThreadImmediateExecutorService
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequestBuilder
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class FetchPictureService : JobIntentService() {

    private val TAG = FetchPictureService::class.java.simpleName
    private lateinit var disposable: CompositeDisposable

    companion object {

        private val JOB_ID = 3078

        fun enqueueWork(context: Context, work: Intent) {
            JobIntentService.enqueueWork(context, FetchPictureService::class.java, JOB_ID, work)
        }
    }

    override fun onCreate() {
        super.onCreate()
        disposable = CompositeDisposable()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun onHandleWork(intent: Intent) {
        val url = intent.getStringExtra("url")

        val builder = NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_ID)

        val bitmap = getPicture(url).blockingGet()

        builder.setContentTitle("title")
            .setContentText("content")
            .setDefaults(Notification.DEFAULT_SOUND)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setChannelId(Constants.NOTIFICATION_CHANNEL_ID)
            .setAutoCancel(true)
            .setLargeIcon(bitmap)

        val jumpIntent = Intent(applicationContext, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext,0,jumpIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification = builder.build()
        notificationManager.notify(0, notification)

    }

    private fun getPicture(url: String): Single<Bitmap> {

        return Single.create { emitter ->

            /**
             * 不指定壓縮尺寸
             */
            // val imageRequest = ImageRequest.fromUri(url)

            /**
             * 指定壓縮尺寸
             */
            val imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url))
                .setResizeOptions(ResizeOptions(256, 256))
                .build()

            val imagePipeline = Fresco.getImagePipeline()
            val dataSource = imagePipeline.fetchDecodedImage(imageRequest, null)
            dataSource.subscribe(
                object : BaseBitmapDataSubscriber() {

                    override fun onNewResultImpl(bitmap: Bitmap?) {
                        Log.e(TAG, "onNewResultImpl")
                        Log.e(TAG, "bitmap width, height = ${bitmap?.width}, ${bitmap?.height}")

                        if (!emitter.isDisposed) {
                            Log.e(TAG, "bitmap recycled =" + bitmap?.isRecycled)
                            emitter.onSuccess(bitmap)
                        }
                    }

                    override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>) {
                        Log.e(TAG, "onFailureImpl")
                        if (!emitter.isDisposed) {
                            emitter.onError(Throwable("onFailureImpl"))
                        }
                    }
                },
                UiThreadImmediateExecutorService.getInstance()
            )
        }
    }

}