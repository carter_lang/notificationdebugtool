package com.example.carter.notificationdemo.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.carter.notificationdemo.R

class MainActivity : AppCompatActivity() {

    /**
     * Notification跳轉，沒有Activity時
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * Notification跳轉，有Activity時
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }
}
